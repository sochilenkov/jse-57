package ru.t1.sochilenkov.tm.exception.system;

import org.jetbrains.annotations.NotNull;

public class ArgumentNotSupportedException extends AbstractSystemException {

    public ArgumentNotSupportedException() {
        super("Error! Argument not supported...");
    }

    public ArgumentNotSupportedException(@NotNull String argument) {
        super("Error! Argument ``" + argument + "`` not supported...");
    }

}
