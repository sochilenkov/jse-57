package ru.t1.sochilenkov.tm.listener;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.sochilenkov.tm.api.ILoggerService;
import ru.t1.sochilenkov.tm.service.LoggerService;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

@Component
public final class LoggerListener implements MessageListener {

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Override
    @SneakyThrows
    public void onMessage(Message message) {
        if (!(message instanceof TextMessage)) return;
        @NotNull final TextMessage textMessage = (TextMessage) message;
        @NotNull final String text = textMessage.getText();
        loggerService.writeLog(text);
    }

}
