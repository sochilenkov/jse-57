package ru.t1.sochilenkov.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.sochilenkov.tm.component.Bootstrap;
import ru.t1.sochilenkov.tm.configuration.ClientConfiguration;

public final class ApplicationClient {

    public static void main(@Nullable String... args) {
        @NotNull ApplicationContext context = new AnnotationConfigApplicationContext(ClientConfiguration.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.run(args);
    }

}