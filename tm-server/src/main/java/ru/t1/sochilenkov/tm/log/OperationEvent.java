package ru.t1.sochilenkov.tm.log;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class OperationEvent {

    @NotNull
    private OperationType type;

    @NotNull
    private Object entity;

    @Nullable
    private String table;

    private long timestamp = System.currentTimeMillis();

    public OperationEvent(@NotNull final OperationType type, @NotNull final Object entity) {
        this.type = type;
        this.entity = entity;
    }

}
